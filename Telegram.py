import requests
import transliterate


class Telegram:
    METHOD_GET_UPDATES = "getUpdates"
    METHOD_SEND_MESSAGE = "sendMessage"
    url = "https://api.telegram.org/bot"
    token = "350506042:AAFXrbIUMBqWC1g98K9L1HmFqdU5H2MpsyE"
    USER_COMMANDS = [
        {
            "command_str": "help",
            "pattern": "/help",
            "description": "Вывод описания текущего бота и списка поддерживаемых команд с их описанием"
        },
        {
            "command_str": "translitru",
            "pattern": "/translitru <ваш текст для транслитерации>",
            "description": "Транслитерация на русский. "
        },
        {
            "command_str": "translituk",
            "pattern": "/translituk <ваш текст для транслитерации>",
            "description": "Транслитерация на украинский"
        },
        {
            "command_str": "translitel",
            "pattern": "/translitel <ваш текст для транслитерации>",
            "description": "Транслитерация на греческий"
        },
    ]

    def __init__(self, token=None):
        if token is not None:
            self.token = token
        self.url = "{url}{token}/".format(url=self.url, token=self.token)

    def send_command(self, command, data):
        url = "{url}{command}".format(url=self.url, command=command)
        data['timeout'] = 100
        response = requests.post(url, data, timeout=100)
        return response.json()

    def send_message(self, chat_id, text):
        return self.send_command(self.METHOD_SEND_MESSAGE, {"chat_id": chat_id, "text": text})

    def get_updates(self, offset=None, limit=None):
        data = {}

        data.update({"limit": limit})

        if offset is None:
            offset = self.get_offset()

        data.update({"offset": offset + 1})

        return self.send_command(self.METHOD_GET_UPDATES, data)

    @staticmethod
    def get_offset():
        try:
            file = open("offset", "r")
            offset = file.read()
            file.close()
        except FileNotFoundError:
            offset = 0
        return int(offset)

    @staticmethod
    def set_offset(offset):
        file = open("offset", "w")
        file.write(str(offset))
        file.close()

    def handle_user_commands(self, chat_id, text):
        if text[0] == "/":
            for user_command in self.USER_COMMANDS:
                command_str = user_command['command_str']
                if text[1:len(command_str) + 1] == command_str:
                    getattr(self, command_str)(command_str, chat_id, text)
                    return
            self.send_message(chat_id, "Неизвестная команда")

    def help(self, command_str, chat_id, text):
        help_str = "Бот предоставляет возможность транслитерации текст в английский и русский текст."
        help_str += " Список доступных комманд:"
        for user_command in self.USER_COMMANDS:
            pattern = user_command['pattern']
            description = user_command['description']
            help_str += "\r\n\r\n" + pattern + "\r\n" + description
        self.send_message(chat_id, help_str)

    def translitru(self, command_str, chat_id, text):
        self.translit(command_str, chat_id, text, text[len("translit") + 1:len(command_str) + 1])

    def translitel(self, command_str, chat_id, text):
        self.translit(command_str, chat_id, text, text[len("translit") + 1:len(command_str) + 1])

    def translituk(self, command_str, chat_id, text):
        self.translit(command_str, chat_id, text, text[len("translit") + 1:len(command_str) + 1])

    def translit(self, command_str, chat_id, text, lang):
        print(lang)
        text = text[len(command_str) + 2:]
        if text == "" or len(text) == 0:
            self.send_message(chat_id, "Вы не ввели текст для транлитерации")
        else:
            self.send_message(chat_id, transliterate.translit(text, lang))

    def init_transliterate(self):
        file = open("check_messages", "w")
        file.write("check_messages")
        file.close()
        self.transliterate_messages()

    def transliterate_messages(self):
        check_messages = True

        while check_messages:
            try:
                open("check_messages", "r")
            except FileNotFoundError:
                check_messages = False

            messages = self.get_updates()
            if messages["ok"] and len(messages['result']):
                for message in messages['result']:
                    self.set_offset(message['update_id'])
                    if 'message' in message:
                        self.handle_user_commands(message['message']['chat']['id'],
                                                  message['message']['text'])
